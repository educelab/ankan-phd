# EduceLab BibTeX References Repository
This repository houses a common set of BibTeX files for use when writing
research papers.

## Usage
### Manual Import
Use the files in this repository however you normally use `.bib` files.
Copy them to your repo. Include this repo as a git submodule. This is the Wild
West. There are no rules here.

### Automatic Import (biblatex only)
This project provides a simple way to provide all BibTeX sources to your
document through a single import using `biblatex` and the `biber`
backend.

First, download a copy of this project and add it as a subdirectory in your
LaTeX project.
Next, import the `biblatex` package and include `add-all-biblatex.tex` in your
primary `.tex` file:

```latex
\usepackage[backend=biber]{biblatex}
\input{add-all-biblatex}
```

Finally, when compiling your document, add `bibtex-refs` to the list of `biber`
input directories:

```shell
biber --input-directory=bibtex-refs/ ...
```

See the [printbib.tex](printbib.tex) and [latexmkrc](latexmkrc) files for a working example of this process.

## Annotated Bibliography
The `printbib.tex` file in this project generates an annotated bibliography of
Ankan's proposal.
The most recent version of this bibliography is available on our team pages
[[Website]](https://educelab.gitlab.io/publications/bibtex-refs)
[[PDF]](https://educelab.gitlab.io/publications/bibtex-refs/DRI%20Team%20Bibliography.pdf).

To manually build the PDF, call `make` from the repository's root. This
requires that recent versions of `lualatex`, `biber`, and `latexmk` are
installed.

## Abstracts
As a convenience, paper abstracts can be included alongside BibTeX entries.
Since this can get unwieldy in the `.bib` files, abstracts should be saved as a
sidecar file in the `abstracts` subdirectory.
Simply follow the naming convention: `bibabstract-{citation key}.tex`.
Abstracts are typeset using LaTeX.
Custom packages must be specified in the `.tex` files that print these abstracts
(e.g. `printbib.tex`).

## Annotations
Annotations for an entry should be placed in the `annotations` subdirectory and
follow the naming convention: `bibannotation-{citation key}.tex`.
They are typeset using LaTeX but are evaluated inside the document's context.
It's a good idea to not use commands from custom packages inside the annotation,
but if you must, custom packages need to be specified in the `.tex` document
that is printing the annotations (e.g. `printbib.tex`).

Annotations should follow an annotated bibliography style.
From [Cornell University Library](https://guides.library.cornell.edu/annotatedbibliography):

>>>
Write a concise annotation that summarizes the central theme and scope of the
book or article. Include one or more sentences that (a) evaluate the authority
or background of the author, (b) comment on the intended audience, (c) compare
or contrast this work with another you have cited, or (d) explain how this work
illuminates your bibliography topic.
>>>
